name := "coronafix"
organization := "iukw2"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  javaJdbc,
  "com.h2database" % "h2" % "1.4.192",
  evolutions,
  javaJpa,
  javaWs,
  ehcache,
  guice,
  "org.hibernate" % "hibernate-core" % "5.4.9.Final",
  "org.postgresql"  % "postgresql"      % "42.2.1"
)
