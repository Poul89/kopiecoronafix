--CardIndex schema

-- !Ups

CREATE TABLE cardIndex (
    id SERIAL NOT NULL PRIMARY KEY,
    title VARCHAR(255),
    category VARCHAR(255),
    description VARCHAR(300)
);

-- !Downs

DROP TABLE cardIndex;