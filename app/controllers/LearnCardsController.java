package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import services.DefaultCardservice;
import views.html.showLearnCards;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class LearnCardsController extends Controller {

/*Status cods
    https://www.playframework.com/documentation/1.2.x/api/play/mvc/Http.StatusCode.html
    https://www.playframework.com/documentation/1.1/api/play/mvc/Controller.html
    */

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    int ce = 444;
    String ha = "Hans";


    // LearnCard service
    DefaultCardservice DBS;
    public Result getAllLearnCards(String LearnCardName, int LearnCardID) {
        if (LearnCardName != null) {
            // String an getLearnCardSearch mitgeben
           return ok(showLearnCards.render(LearnCardName, LearnCardID));
          /*  for (LearnCard bo : bo)
                if (LearnCard.getName.equals(LearnCardName)) {
                    return bo;
                }
            */
        } else {
        //    return ok(getAllLearnCards.render());
            // return LearnCard;
            return ok();
        }

    }

    public Result postLearnCardDetail(int id) {
        if (id <= 0) {
            return notFound();
        } else {
       //     return ok(postLearnCardDetail.render());
            return ok();
        }
    }

    public Result putLearnCard(int id) {

      //  return ok(putLearnCardDetailForLearnCardWithID.render());
        return ok();
    }

    public Result getLearnCard(int id) {

      // return ok(getLearnCardDetailWithLearnCardID.render(id));
        return ok();
    }

    /*public Result getLearnCardSearch() {

        return ok(views.html.getLearnCardSearch.render());
          return ok();
    }
     */
    public Result deleteLearnCardWithID(int id) {

     //   return ok(deleteLearnCardWithID.render());
        return ok();
    }
}

