package controllers;

import com.fasterxml.jackson.databind.JsonNode;

import play.core.j.HttpExecutionContext;
import play.libs.Json;
import play.mvc.*;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import models.CardIndex;
import services.CardIndexService;
import services.DefaultCardIndexService;
import javax.inject.Inject;



public class CardIndexController extends Controller {


    private final CardIndexService cardIndexService;

    @Inject
    public CardIndexController(CardIndexService cardIndexService) {
        this.cardIndexService = cardIndexService;
    }

    public CompletionStage<Result> getAllCardIndex(String q) {
        return cardIndexService.get().thenApplyAsync(cardIndexStream ->
                ok(Json.toJson(cardIndexStream.collect(Collectors.toList())))
        );
    }

    public CompletionStage<Result> getCardIndex(Long id) {
        return cardIndexService.get(id).thenApplyAsync(cardIndex -> ok(Json.toJson(cardIndex)));
    }

    /*public CompletionStage<Result> search(String title) {
            return ok("Results for " + title);
    }
    */

    public CompletionStage<Result> addCardIndex(Http.Request request) {
        JsonNode json = request.body().asJson();
        CardIndex AddcardIndex = Json.fromJson(json, CardIndex.class);
        return cardIndexService.add(AddcardIndex).thenApplyAsync(cardIndex -> ok(Json.toJson(cardIndex)));
    }

    //wird hier direkt das Json geliefert oder muss man die id noch mitliefern?
    public CompletionStage<Result> updateCardIndex(Long id, Http.Request request) {
        JsonNode json = request.body().asJson();
        CardIndex updateCardIndex = Json.fromJson(json, CardIndex.class);
        updateCardIndex.setId(id);
        return cardIndexService.update(updateCardIndex).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    public CompletionStage<Result> deleteCardIndex(Long id) {
        return cardIndexService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }
}

