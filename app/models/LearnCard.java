package  models;

public class LearnCard {

        private int id;
        private String name;
        private String isbn13;
        private String isbn10;
        private String description;
        private String publisher;
        private int pages;

        public int getId() {
            return id;
        }

        public int getPages() {
            return pages;
        }

        public String getDescription() {
            return description;
        }

        public String getIsbn10() {
            return isbn10;
        }

        public String getIsbn13() {
            return isbn13;
        }

        public String getName() {
            return name;
        }

        public String getPublisher() {
            return publisher;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setIsbn10(String isbn10) {
            this.isbn10 = isbn10;
        }

        public void setIsbn13(String isbn13) {
            this.isbn13 = isbn13;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public void setPublisher(String publisher) {
            this.publisher = publisher;
        }

    }
