package models;

import play.db.jpa.JPAApi;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;
import static java.util.concurrent.CompletableFuture.supplyAsync;

public class CardIndexRepository {

    private final JPAApi jpaApi;
    @Inject
    public CardIndexRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<CardIndex> add(CardIndex cardIndex) {
        return supplyAsync(() -> wrap(em -> insert(em, cardIndex)));
    }

    public CompletionStage<CardIndex> update(CardIndex cardIndex) {
        return supplyAsync(() -> wrap(em -> update(em, cardIndex)));
    }

    public CompletionStage<CardIndex> find(Long id) {
        return supplyAsync(() -> wrap(em -> find(em, id)));
    }

    public CompletionStage<Boolean> remove(Long id) {
        return supplyAsync(() -> wrap(em -> remove(em, id)));
    }

    public CompletionStage<Stream<CardIndex>> list() {
        return supplyAsync(() -> wrap(em -> list(em)));
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }
    private CardIndex insert(EntityManager em, CardIndex cardIndex) {
        em.persist(cardIndex);
        return cardIndex;
    }

    private CardIndex update(EntityManager em, CardIndex cardIndex) {
        CardIndex updatedCardIndex = em.find(CardIndex.class, cardIndex.getId());
        updatedCardIndex.setTitle(cardIndex.getTitle());
        updatedCardIndex.setCategory(cardIndex.getCategory());
        updatedCardIndex.setDescription(cardIndex.getDescription());
        return updatedCardIndex;
    }

    private CardIndex find(EntityManager em, Long id) {
        return em.find(CardIndex.class, id);
    }


    private Boolean remove(EntityManager em, Long id) {
        CardIndex removedCardIndex = em.find(CardIndex.class, id);
        if(null != removedCardIndex) {
            em.remove(removedCardIndex);
            return true;
        }
        return false;
    }

    private Stream<CardIndex> list(EntityManager em){
        List<CardIndex> books = em.createQuery("select c from cardIndex c", CardIndex.class).getResultList();
        return books.stream();
    }
}
