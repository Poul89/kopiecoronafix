package services;

import models.CardIndex;
import models.CardIndexRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import javax.inject.Inject;

public class DefaultCardIndexService implements CardIndexService{

    private CardIndexRepository cardIndexRepository;

    @Inject
    public DefaultCardIndexService(CardIndexRepository cardIndexRepository) {
        this.cardIndexRepository = cardIndexRepository;
    }

    public CompletionStage<Stream<CardIndex>> get() {
        return cardIndexRepository.list();
    }

    public CompletionStage<CardIndex> get(final Long id) {
        return cardIndexRepository.find(id);
    }

    public CompletionStage<Boolean> delete(final Long id) {
        return cardIndexRepository.remove(id);
    }

    public CompletionStage<CardIndex> update(final CardIndex updatedCardIndex) {
        return cardIndexRepository.update(updatedCardIndex);
    }

    public CompletionStage<CardIndex> add(final CardIndex cardIndex) {
        return cardIndexRepository.add(cardIndex);
    }
}
