package services;

import com.google.inject.ImplementedBy;
import models.CardIndex;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
@ImplementedBy(DefaultCardIndexService.class)

public interface CardIndexService {

    /**
     * Returns List of all Card Indexes
     * @return list of all Card Indexes
     */
    CompletionStage<Stream<CardIndex>> get();

    /**
     * Returns Card Index with given identifier.
     * @param id cardIndex identifier
     * @return book with given identifier or {@code null}
     */
    CompletionStage<CardIndex> get(final Long id);

    /**
     * Removes Card Index with given identifier.
     * @param id cardIndex identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Updates Card Index with given identifier.
     * @param updaetdCardIndex book with updated fields
     * @return updated CardIndex
     */
    CompletionStage<CardIndex> update(final CardIndex updaetdCardIndex);

    /**
     * Adds the given Card Index.
     * @param cardIndex to add
     * @return added book
     */
    CompletionStage<CardIndex> add(final CardIndex cardIndex);
}
