package services;

import java.util.List;
import models.LearnCard;


public class DefaultCardservice extends LearnCard {
    /**
     * Return's list of all cards.
     * @return list of all cards
     */
    private  List<LearnCard> cards;
    public  List<LearnCard> get() {
        return cards;
    };
    /**
     * Returns LearnCards with given identifier.
     * @param id LearnCards identifier
     * @return LearnCards with given identifier or {@code null}
     */
    public LearnCard get(final int id){

        for (LearnCard i: cards){
            if(i.getId() == id) {
                return i;
            }
        }
        return null;
    };
    /**
     * Removes LearnCards with given identifier.
     * @param id LearnCards identifier
     * @return {@code true} on success {@code false} on failure
     */
    public  boolean delete(final int id){

        for (LearnCard i: cards){
            if(i.getId() == id) {
                cards.remove(i);
                return true;
            }
        }
        return false;
    };
    /*
     *
     * Updates LearnCards with given identifier.
     * @param updatedLearnCards LearnCards with updated fields
     * @return updated LearnCards
     */
    public LearnCard update(final LearnCard updatedLearnCard){
        for (LearnCard i: cards){
            if(i.getId() == updatedLearnCard.getId()) {
                cards.remove(i);
                cards.add(updatedLearnCard);
            }
        }
        return updatedLearnCard;
    };
    /**
     * Adds the given LearnCards.
     * @param LearnCards to add
     * @return added LearnCards
     */
    public LearnCard add(final LearnCard LearnCard){
        LearnCard newLearnCard;
        cards.add(LearnCard);
        newLearnCard = cards.get(cards.indexOf(LearnCard));
        return newLearnCard;
    };
}